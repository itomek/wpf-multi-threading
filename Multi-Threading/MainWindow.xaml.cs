﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace Multi_Threading
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

			// Look at the .NET 4 version that uses Tasks

			UsingBackgroundWorker(); // using System.Thread

			UsingThreading(); // using System.ComponentModel
		}

		#region Regular Threading

		// requires System.Threading AND System.Windows.Threading
		private void UsingThreading()
		{
			ThreadStart myThread = delegate()
			{
				for (int i = 0; i < 1000; i++)
				{
					Thread.Sleep(100);

					Dispatcher.Invoke(DispatcherPriority.Normal, new Action<int>(Update), i);
				}
			};

			new Thread(myThread).Start();
		}

		private void Update(int i)
		{
			this.progRegularThreading.Value = i;
		}

		#endregion

		#region Background Worker

		private void UsingBackgroundWorker()
		{
			BackgroundWorker worker = new BackgroundWorker();
			worker.WorkerReportsProgress = true;
			worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
			worker.DoWork += new DoWorkEventHandler(worker_ProgressChanged);
			worker.RunWorkerAsync();
		}

		// called by delegate that is processing the change
		protected void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			this.progBackgroundThread.Value = e.ProgressPercentage;
		}

		// called by delegate that performs the work
		private void worker_ProgressChanged(object sender, DoWorkEventArgs e)
		{
			BackgroundWorker worker = sender as BackgroundWorker;

			for (int i = 0; i < 1000; i++)
			{
				Thread.Sleep(100);
				worker.ReportProgress(i);
			}
		}

		#endregion

	}
}
