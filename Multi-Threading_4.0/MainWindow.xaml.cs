﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Multi_Threading_4._0
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private int _counter = 0;

		public MainWindow()
		{
			InitializeComponent();

			//Task myTask = Task.Factory.StartNew(() => this.DoCounter());
			Task myTask = new Task(() => this.DoCounter());


			Task myNewTask = new Task(() =>
			{
				while (this._counter != 1000)
					Dispatcher.Invoke(DispatcherPriority.Normal, new Action<int>(UpdateProgBar), this._counter);
			});

			myTask.Start();
			myNewTask.Start();
		}

		private void UpdateProgBar(int increment)
		{
			this.progTaskThreading.Value = this._counter;
		}

		private void DoCounter()
		{
			for (int i = 0; i != 1000; i++)
			{
				this._counter++;

				Thread.Sleep(100);
			}
		}
	}
}
